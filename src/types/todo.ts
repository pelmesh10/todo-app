interface TodoItem {
  text: string,
  isCompleted: boolean,
  isBeingEdited: boolean,
}

export default TodoItem;
