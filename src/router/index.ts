// Composables
import { createRouter, createWebHistory } from 'vue-router';

const routes = [
  {
    path: '/',
    component: () => import('@/layouts/Default.vue'),
    children: [
      {
        path: '',
        name: 'TodoList',
        component: () => import('@/views/TodoList.vue'),
      },
      {
        path: '/:pathMatch(.*)',
        name: 'NotFound',
        component: () => import('@/views/NotFound.vue'),
      },
    ],
  },
];

const router = createRouter({
  history: createWebHistory('/todo-app/'),
  routes,
});

export default router;
